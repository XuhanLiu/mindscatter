# mind-scatter

#### Introductions
The scatter operators are implemented with Mindspore AI framework, who's API is equivalent to torch-scatter under PyTorch.

#### Dependencies

Mindspore >= 1.8


#### Parameters
src – The source tensor.

index – The indices of elements to scatter.

axis – The axis along which to index. (default: -1)

out – The destination tensor.

n_axis – If out is not given, automatically create output with size n_axis at dimension dim. If n_axis is not given, a minimal sized output tensor according to index.max() + 1 is returned.

reduce – The reduce operation ("add", "mul", "mean", "min" or "max", "update"). (default: "update")


#### Examples

>>> from scatter import scatter

>>> import mindspore.numpy as np

>>> import mindspore as ms

>>> src = np.randn(10, 6, 64)

>>> index = ms.Tensor([0, 1, 0, 1, 2, 1])

>>> out = scatter(src, index, axis=1, reduce="sum")

>>> print(out.size())

Results:
>>> torch.Size([10, 3, 64])

#### References
https://pytorch-scatter.readthedocs.io/en/latest/functions/scatter.html
